class Animal {
    constructor(name){
    	this.legs = 4;
    	this.cold_blooded = false;
    	this.name = name;
    }
    get cnam(){
    	return this.name;
    }
    set cnam(x){
    	this.name=x;
    }
}
 
var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false


class sungokong extends Animal{
	constructor(name){
		super(name);
	}
	jump(){
		console.log(Auooo)
	}
}

class Frog extends Animal{
	constructor(name){
		super(name);
	}
	jump(){
		console.log(hop hop)
	}
}



var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"
 
var kodok = new Frog("buduk")
kodok.jump() // "hop hop