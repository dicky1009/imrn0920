//1
// function teriak() {
//   return "Halo Sanbers!"
// }
 
// var muncul = teriak();
// console.log(muncul)

//2
// function kalikan(num1, num2) {
//   return num1 *  num2
// }

// var num1 = 12
// var num2 = 4
 
// var hasilKali = kalikan(num1, num2)
// console.log(hasilKali)

//3
function introduce(name , age, address, hobby){
	var string = "Nama saya " + name + ", umur saya " + age + " tahun," + " alamat saya di " + address + ", dan saya punya hobby yaitu " 
	+ hobby + "!"
	return string
}

var name = "Agus"
var age = 30
var address = "Jln. Malioboro, Yogyakarta"
var hobby = "Gaming"
 
var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan) // Menampilkan "Nama saya Agus, umur saya 30 tahun, alamat saya di Jln. Malioboro, Yogyakarta, dan saya punya hobby yaitu Gaming!" 